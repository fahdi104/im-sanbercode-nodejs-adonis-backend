"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sapa = exports.filterData = exports.convert = exports.checkScore = void 0;

//soal 1
var sapa = function sapa(nama) {
  return "Halo, selamat pagi ".concat(nama);
}; //soal 2


exports.sapa = sapa;

var convert = function convert(nama, domisili, umur) {
  return {
    nama: nama,
    domisili: domisili,
    umur: umur
  };
}; //soal 3


exports.convert = convert;

var checkScore = function checkScore(str) {
  var splitStr = str.split(",");
  var valueArr = [];

  for (var i = 0; i < splitStr.length; i++) {
    var value = splitStr[i].split(":")[1];
    valueArr.push(value);
  }

  var name = valueArr[0],
      kelas = valueArr[1],
      score = valueArr[2];
  return {
    name: name,
    kelas: kelas,
    score: score
  };
}; //soal 4
//filter data/search data


exports.checkScore = checkScore;
var data = [{
  name: "Ahmad",
  kelas: "adonis"
}, {
  name: "Regi",
  kelas: "laravel"
}, {
  name: "Bondra",
  kelas: "adonis"
}, {
  name: "Iqbal",
  kelas: "vuejs"
}, {
  name: "Putri",
  kelas: "Laravel"
}];

var filterData = function filterData(kelas) {
  return data.filter(function (el) {
    return el['kelas'].toLowerCase().includes(kelas.toLowerCase());
  });
};

exports.filterData = filterData;