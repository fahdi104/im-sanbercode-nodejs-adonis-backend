"use strict";

var _soal = require("./lib/soal.js");

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var myArgs = process.argv.slice(2);

switch (myArgs[0]) {
  case 'sapa':
    var nama = myArgs[1];
    console.log((0, _soal.sapa)(nama));
    break;

  case 'convert':
    //assign
    var param = myArgs.slice(1); //assign var via destucturing

    var _param = _slicedToArray(param, 3),
        name = _param[0],
        domisili = _param[1],
        umur = _param[2];

    console.log((0, _soal.convert)(name, domisili, umur));
    break;

  case 'checkScore':
    //assign
    var studentString = myArgs[1]; //assign var via destucturing

    console.log((0, _soal.checkScore)(studentString));
    break;

  case 'filterData':
    var filter = myArgs[1];
    console.log((0, _soal.filterData)(filter));
    break;

  default:
    console.log('Sorry, that is not something I know how to do.');
}