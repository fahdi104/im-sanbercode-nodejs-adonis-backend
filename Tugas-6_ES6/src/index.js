import {sapa,convert,checkScore,filterData} from "./lib/soal.js"

const myArgs = process.argv.slice(2);


switch (myArgs[0]) {
  case 'sapa':
    let nama=myArgs[1];
    console.log(sapa(nama))
    break;

 case 'convert':
    
    //assign
    const param=myArgs.slice(1);

    //assign var via destucturing
    let[name,domisili,umur] = param
    console.log(convert(name,domisili,umur))
    break;

  case 'checkScore':
    
    //assign
     let studentString=myArgs[1];
    
    //assign var via destucturing
    console.log(checkScore(studentString))

    break;
 
  case 'filterData':
    
    let filter=myArgs[1];
    console.log(filterData(filter))

    break;

  default:
    console.log('Sorry, that is not something I know how to do.');
}