//soal 1
export const sapa = (nama) => {
    return `Halo, selamat pagi ${nama}`
}

//soal 2
export const convert = (nama, domisili,umur) => {
    return {
        nama,
        domisili,
        umur
    }
}

//soal 3
export const checkScore = (str) => {
    let splitStr = str.split(",")
    let valueArr = []
    for (var i=0; i<splitStr.length; i++){
        var value = splitStr[i].split(":")[1]
        valueArr.push(value)
    }
    let [name,kelas,score] = valueArr;
    return {name,kelas,score}
}

//soal 4
//filter data/search data
const data = [
  { name: "Ahmad", kelas: "adonis"},
  { name: "Regi", kelas: "laravel"},
  { name: "Bondra", kelas: "adonis"},
  { name: "Iqbal", kelas: "vuejs" },
  { name: "Putri", kelas: "Laravel" }
]

export const filterData = (kelas) => {

return data.filter((el) => el['kelas'].toLowerCase().includes(kelas.toLowerCase()));

}