function arrayToObject(arr) {
    // exception

    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)

    if(arr.length<=0){
        console.log("Array input is empty")
    }else{
        // start convert
        for(var i = 0; i < arr.length; i++) {
            
            var personObject={}
            var detailObject={}

            var age = thisYear-arr[i][3]
            if(age<=0 || isNaN(age)){
                age="Invalid birth year"
            }

            detailObject['firstName'] = arr[i][0];
            detailObject['lastName'] = arr[i][1];
            detailObject['gender'] = arr[i][2];
            detailObject['age'] = age;

            personObject[arr[i][0]+" "+arr[i][1]] = detailObject
            
            //display output
            console.log(i+1+".")
            console.log(personObject)

        }
    }
} 

 // Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""