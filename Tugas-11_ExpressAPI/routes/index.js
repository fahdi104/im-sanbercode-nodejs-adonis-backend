var express = require('express');
var router = express.Router();

const Karyawan = require('../controllers/controllerKaryawan')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

//release 0
router.post('/register', Karyawan.register)

//release 1
router.get('/karyawan', Karyawan.list)

//post 
router.post('/login', Karyawan.login)

//Endpoint : POST /karyawan/:name/siswa
router.post('/karyawan/:name/siswa', Karyawan.addSiswa)

module.exports = router;
