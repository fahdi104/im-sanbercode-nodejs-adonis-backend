const fs = require('fs')
const fsPromises = require('fs/promises')

const path = './data.json'

class Karyawan {
    static async register(req, res) {
        //res.status(200).json({message:"testing"})

        try {
            let readFile = await fsPromises.readFile(path)
            let existingData = JSON.parse(readFile)

            let { name, role, password } = req.body
            console.log(req.body)

            let newKaryawan = { name, role, password, isLogin: false }

            existingData.push(newKaryawan)

            await fsPromises.writeFile(path, JSON.stringify(existingData));
            res.status(201).json({ message: "Berhasil Register" })
        } catch (err) {
            res.status(400).json({ message: "Read Data Error" })
        }

    } // end register

    static async list(req, res) {

        try {
            let readFile = await fsPromises.readFile(path)
            let existingData = JSON.parse(readFile)
            res.status(201).json(existingData)
        } catch (err) {
            res.status(400).json({ message: "Read Data Error" })
        }

    } // end list

    static async login(req, res) {
        //get username and password from req.body
        let { name, password } = req.body

        //open data and find that username
        try {
            let readFile = await fsPromises.readFile(path)
            let existingData = JSON.parse(readFile)

            //check if password is correct
            let karyawanID = existingData.findIndex((Karyawan) => Karyawan.name === name)
            let employee = existingData[karyawanID]
            if (karyawanID == -1) {
                console.log("Error: Data tidak ditemukan")
            } else if (employee.name == name && employee.password == password) {
                employee.isLogin = true;

                //write to file
                existingData.splice(karyawanID, 1, employee)
                await fsPromises.writeFile(path, JSON.stringify(existingData));
                res.status(201).json({ message: "Berhasil Login" })
            } else {
                res.status(400).json({ message: "Username and/or Password is not correct" })
            }

        } catch (err) {
            res.status(400).json({ message: "Read Data Error" })
        }

    } // end login

    static async addSiswa(req, res) {
        //get data from body

        try {
            //open data and find admin
            let readFile = await fsPromises.readFile(path)
            let existingData = JSON.parse(readFile)

            //check if Admin is login
            let karyawanID = existingData.findIndex((Karyawan) => Karyawan.role === 'admin')
            let admin = existingData[karyawanID]

            //check if Trainer is found and load
            let { name } = req.params
            let trainerID = existingData.findIndex((Karyawan) => Karyawan.name === name)
            let trainerEmp = existingData[trainerID]

            if (karyawanID == -1) {
                res.status(400).json({ message: "Admin is not found" })
            } else if (admin.role == 'admin' && admin.isLogin == true) {
                //catch error if trainer not found
                if (trainerID == -1) {
                    res.status(400).json({ message: "Trainer is not found" })
                } else {
                    //add data to file
                    let { name, kelas } = req.body

                    //dumb hack to init empty array
                    let newStudent = []
                    if ('students' in trainerEmp) {
                        newStudent = { ...trainerEmp }
                    } else {
                        let students = []
                        newStudent = { ...trainerEmp, students }
                    }
                    newStudent.students.push({ name, kelas })
                    existingData.splice(trainerID, 1, newStudent)

                    await fsPromises.writeFile(path, JSON.stringify(existingData));
                    res.status(200).json({ message: "Berhasil add siswa" })
                }
            } else {
                res.status(400).json({ message: "Admin is not login" })
            }

        } catch (err) {
            res.status(400).json({ message: "Read Data Error" })
        }

    } // end addSiswa
}

module.exports = Karyawan