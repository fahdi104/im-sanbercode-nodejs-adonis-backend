function kelompokAngka(arr){

//- kelompok pertama: angkaganjil
//- kelompok kedua: angka genap 
//- kelompok tiga: angka kelipatan tiga

	var output=[]
	var ganjil=[]
	var genap=[]
	var lipat3=[]

	for(var i = 0; i <= arr.length-1; i++) {
	
	if (arr[i] % 2 != 0 && arr[i] % 3 != 0){
    	ganjil.push(arr[i])		
		} else if (arr[i] % 2 == 0 && arr[i] % 3 != 0) { 
    	genap.push(arr[i])
  		} else if (arr[i] % 3 == 0) { 
    	lipat3.push(arr[i])
  		}
	}
	output=[ganjil,genap,lipat3];
	return output 
}
//Test Case
console.log(kelompokAngka([1,2,3,4,5,6,7]))
//Output:[[1,5,7],[2,4],[3,6]]
console.log(kelompokAngka([13,27,11,15]))
//Output:[[13,11],[],[27,15]]
console.log(kelompokAngka([]))
//output:[[],[],[]]