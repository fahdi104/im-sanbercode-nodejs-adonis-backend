function graduate(arr) {

      var output = {}

      for(var i = 0; i < arr.length; i++) {
  

        var detailObject={}

        var grade = arr[i]['score'];
        var msg_grade =''

		switch (true) {
    		case (grade < 60):
        	msg_grade = "participated"
        	break;
    		case (grade>60 && grade<85):
        	msg_grade = "completed"
        	break;
        	case (grade>85):
        	msg_grade = "mastered"
        	break;
		}

        if (!(arr[i]['class'] in output)){
          detailObject['name']=arr[i]['name'];
          detailObject['score']=arr[i]['score'];
          detailObject['grade']=msg_grade;

          output[arr[i]['class']] = [detailObject];

        }else
        {
          detailObject['name']=arr[i]['name'];
          detailObject['score']=arr[i]['score'];
          detailObject['grade']=msg_grade;

          output[arr[i]['class']].push(detailObject);
         }
     } 
  return output
}

// TEST CASE
// Contoh Input

var arr = [
{name:"Ahmad",score:80, class: "Laravel"},
{name:"Regi",score:86, class: "Vuejs"},
{name:"Robert",score:59, class: "Laravel"},
{name:"Bondra",score:81, class: "Reactjs" }
]
console.log(graduate(arr))

//Output
//{
//Laravel:[{name:"Ahmad",score:80, grade: "completed"}, { name: "Robert", score: 59, grade: "participated"}],
//Vuejs:[
//{name:"Regi",score:86, grade: "mastered"}
//],
//Reactjs:[{name:"Bondra",score:81, grade: "completed"}]
//}