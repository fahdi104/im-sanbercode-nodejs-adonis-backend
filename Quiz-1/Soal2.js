function hitungVokal(str){
	vokal = ['a','i','u','e','o']

	i = 0;

    for (var huruf of str.toLowerCase()) {
        if (vokal.includes(huruf)) {
            i++;
        }
    }

    // return number of vowels
    return i
}

// Test Cases
console.log(hitungVokal("Adonis"))// Output:3
console.log(hitungVokal("Error"))//Output:2
console.log(hitungVokal("Eureka"))//Output:4
console.log(hitungVokal("Rsch")) // Output: 0