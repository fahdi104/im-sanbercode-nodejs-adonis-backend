/*
No 1

Nomor ID:  0001
Nama Lengkap:  Roman Alamsyah
TTL:  Bandar Lampung 21/05/1989
Hobi:  Membaca

*/

console.log("Soal 1")


function dataHandling(input) {
	var nrow = input.length
	var ncol = input[0].length
	
	var label = ["Nomor ID", "Nama Lengkap", "TTL", "Hobi"]
	
	for(var i = 0; i < nrow; i++) {
			console.log(label[0]+" : "+input[i][0])
			console.log(label[1]+" : "+input[i][1])
			console.log(label[2]+" : "+input[i][2]+" "+input[i][3])
			console.log(label[3]+" : "+input[i][4])
			console.log("\n")
      }  
}

//contoh input
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]

dataHandling(input)


/*
No 1

Nomor ID:  0001
Nama Lengkap:  Roman Alamsyah
TTL:  Bandar Lampung 21/05/1989
Hobi:  Membaca

*/
console.log("")
console.log("Soal 2")

function balikKata(input){
	var step=input.length-1; //array start from 0
	var output = '';
	
	while(step >= 0) { 

		output += input[step]
		step--
	}
	return "Output:" +output
}

console.log(balikKata("SanberCode")) 
//Output: edoCrebnaS

console.log(balikKata("racecar")) 
//Output: racecar

console.log(balikKata("kasur rusak"))
//Output: kasur rusak

console.log(balikKata("haji ijah"))
//Output: haji ijah

console.log(balikKata("I am Sanbers"))
//Output: srebnaS ma I