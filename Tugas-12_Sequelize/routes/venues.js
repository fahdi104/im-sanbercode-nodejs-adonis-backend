var express = require('express');
var router = express.Router();

const VenuesContollers = require('../controllers/controllerVenues')

//release1
//under venues js router, get /, means url /venues/

//Endpoint 1 : POST /venues Menyimpan data venue baru yang menangkap data dari request body.
router.post('/', VenuesContollers.store)

//Endpoint 2 : GET /venues Menampilkan seluruh data venue
router.get('/', VenuesContollers.list)

//Endpoint 3 : GET /venues/:id  Menampilkan data venue dengan id tertentu.
router.get('/:id', VenuesContollers.list)

//release 2
//Endpoint 4  : PUT /venues/:id mengubah data venue dengan id sesuai parameter
router.put('/:id', VenuesContollers.update)

//Endpoint 5 : DELETE /venues/:id menghapus data venue dengan id sesuai parameter
router.delete('/:id', VenuesContollers.delete)

module.exports = router;
