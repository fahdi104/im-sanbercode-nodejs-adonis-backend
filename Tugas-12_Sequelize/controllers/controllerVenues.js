const { where } = require('sequelize');
const { Venues } = require('../models')

class VenuesContollers {

    static async store(req, res) {
        try {
            let inputName = req.body.name
            let inputAddress = req.body.address
            let inputPhone = req.body.phone

            // Create a new Venues
            const venues = await Venues.create({
                name: inputName,
                address: inputAddress,
                phone: inputPhone
            });

            res.status(200).json({
                status: "Success",
                message: "Venues berhasil ditambahkan"
            })
        } catch (error) {
            res.status(401).json({
                status: "Failed",
                message: "Tidak dapat menambahkan data"
            })
        }
    }

    static async list(req, res) {
        let id = req.params.id
        try {
            if (id == null) {
                console.log("id is null")
                var venues = await Venues.findAll();
            } else {
                var venues = await Venues.findAll({
                    where: {
                        id: id
                    }
                });
            }

            if (venues.length > 0) {
                res.status(200).json({
                    status: "Success",
                    data: venues
                })
            } else {
                res.status(401).json({
                    status: "Failed",
                    message: "Tidak ada data"
                })
            }

        } catch (error) {
            res.status(401).json({
                status: "Failed",
                message: "Tidak dapat menampilkan data"
            })
        }
    }// end list

    static async update(req, res) {
        let id = req.params.id
        if (id != null) {
            try {
                let inputName = req.body.name
                let inputAddress = req.body.address
                let inputPhone = req.body.phone
                await Venues.update({
                    name: inputName,
                    address: inputAddress,
                    phone: inputPhone
                }, {
                    where: {
                        id: id
                    }
                });
                res.status(200).json({
                    status: "Success",
                    message: "Data diupdate", id
                })
            } catch (error) {
                res.status(401).json({
                    status: "Failed",
                    message: "Tidak dapat mengupdate data"
                })
            }
        } else {
            res.status(401).json({
                status: "Failed",
                message: "Tidak ada data"
            })
        }
    }// end update

    static async delete(req, res) {
        let id = req.params.id
        if (id != null) {
            try {
                await Venues.destroy({
                    where: {
                      id: id
                    }
                  });

                res.status(200).json({
                    status: "Success",
                    message: "Data didelete", id
                })
                
            } catch (error) {
                res.status(401).json({
                    status: "Failed",
                    message: "Tidak dapat mendelete data"
                })
            }
        } else {
            res.status(401).json({
                status: "Failed",
                message: "Tidak ada data"
            })
        }
    }// end delete

    static test(req, res) {
        res.status(200).json({
            status: "testing",
        })
    }

}// end class

module.exports = VenuesContollers