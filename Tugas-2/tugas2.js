/* 
Soal No. 1 (Membuat kalimat), 

*/
console.log("Soal 1")

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
var space = ' '

var sentence1 = word + space + second + space + third + space + fourth + space + fifth + space + 
	sixth + space + seventh
console.log(sentence1)

/* 
Soal No.2 Mengurai kalimat (Akses karakter dalam string), 

*/
console.log(" ")
console.log("Soal 2")


var sentence = "I am going to be React Native Developer"; 

var FirstWord = sentence[0]; 
var SecondWord = sentence[2] + sentence[3]; 
var ThirdWord = sentence[5]+sentence[6]+sentence[7]+sentence[8]+sentence[9]; 
var FourthWord = sentence[11]+sentence[12];
var FifthWord = sentence[14]+sentence[15]; 
var SixthWord = sentence[17]+sentence[18]+sentence[19]+sentence[20]+sentence[21]; 
var SeventhWord = sentence[22]+sentence[23]+sentence[24]+sentence[25]+sentence[26]+sentence[27]+sentence[28];
var EightWord = sentence[30]+sentence[31]+sentence[32]+sentence[33]+sentence[34]+sentence[35]+sentence[36]+
	sentence[37]+sentence[38];

console.log('First Word: ' + FirstWord); 
console.log('Second Word: ' + SecondWord);
console.log('Third Word: ' + ThirdWord); 
console.log('Fourth Word: ' + FourthWord); 
console.log('Fifth Word: ' + FifthWord); 
console.log('Sixth Word: ' + SixthWord); 
console.log('Seventh Word: ' + SeventhWord); 
console.log('Eighth Word: ' + EightWord);

/* 
Soal No. 3 Mengurai Kalimat (Substring)

*/
console.log(" ")
console.log("Soal 3")

var sentence2 = 'wow JavaScript is so cool'; 

var FirstWord2 = sentence2.substring(0, 3); 
var SecondWord2 = sentence2.substring(4, 14);   
var ThirdWord2 = sentence2.substring(15,17);  
var FourthWord2 = sentence2.substring(18, 20); 
var FifthWord2 = sentence2.substring(21, 25); ;  

console.log('First Word: ' + FirstWord2); 
console.log('Second Word: ' + SecondWord2); 
console.log('Third Word: ' + ThirdWord2); 
console.log('Fourth Word: ' + FourthWord2); 
console.log('Fifth Word: ' + FifthWord2);

/* 
Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String

*/
console.log(" ")
console.log("Soal 4")

var sentence3 = 'wow JavaScript is so cool'; 

var FirstWord3 = sentence3.substring(0, 3); 
var SecondWord3 = sentence3.substring(4, 14);   
var ThirdWord3 = sentence3.substring(15,17);  
var FourthWord3 = sentence3.substring(18, 20); 
var FifthWord3 = sentence3.substring(21, 25); ;   

var FirstWordLength = FirstWord3.length  
var SecondWordLength = SecondWord3.length  
var ThirdWordLength = ThirdWord3.length  
var FourthWordLength = FourthWord3.length  
var FifthLength = FifthWord3.length  


console.log('First Word: ' + FirstWord3 + ', with length: ' + FirstWordLength); 
console.log('Second Word: ' + SecondWord3 + ', with length: ' + SecondWordLength); 
console.log('Third Word: ' + ThirdWord3 + ', with length: ' + ThirdWordLength); 
console.log('Fourth Word: ' + FourthWord3 + ', with length: ' + FourthWordLength); 
console.log('Fith Word: ' + FifthWord3 + ', with length: ' + FifthLength); 



/* 
Soal No. 5
B. Tugas Conditional

If Else

*/
console.log(" ")
console.log("B. Tugas Conditional - If Else")

// Output untuk Input nama = '' dan peran = ''
var nama = ""
var peran = ""

if ( nama.length == 0 ) {
    var msg = "Nama harus diisi!"
} else {
	var msg = "Halo " +peran+" "+nama
}
console.log(msg)

//Output untuk Input nama = 'John' dan peran = ''
var nama = "John"
var peran = ""

if ( nama.length == 0 ) {
    var msg_nama = "Nama harus diisi!"
} else {
	var msg_nama = "Halo "+nama
}

if ( peran.length == 0 ) {
    var msg_peran = "Pilih peranmu untuk memulai game!"
} 

console.log(msg_nama +", "+ msg_peran)

//Output untuk Input nama = 'Jane' dan peran 'Penyihir'

var nama = "Jane"
var peran = "Penyihir"

if ( nama.length == 0 ) {
    var msg_nama = "Nama harus diisi!"
} else {
	var msg_nama = "Selamat Datang di dunia Werewolf, "+nama
}

if ( peran.length == 0 ) {
    var msg_peran = "Pilih peranmu untuk memulai game!"
} else if (peran == "Penyihir") {
	var msg_peran = "Halo "+peran+" "+nama+" kamu dapat melihat siapa yang menjadi Werewolf!"
} else if (peran == "Guard") {
	var msg_peran = "Halo "+peran+" "+nama+" kamu akan membantu melindungi temanmu dari serangan Werewolf!"
} else if (peran == "Werewolf") {
	var msg_peran = "Halo "+peran+" "+nama+" kamu akan memakan mangsa setiap malam!"
}

//"Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
console.log(msg_nama)
console.log(msg_peran)



//Output untuk Input nama = 'Jenita' dan peran 'Guard'
var nama = "Jenita"
var peran = "Guard"

if ( nama.length == 0 ) {
    var msg_nama = "Nama harus diisi!"
} else {
	var msg_nama = "Selamat Datang di dunia Werewolf, "+nama
}

if ( peran.length == 0 ) {
    var msg_peran = "Pilih peranmu untuk memulai game!"
} else if (peran == "Penyihir") {
	var msg_peran = "Halo "+peran+" "+nama+" kamu dapat melihat siapa yang menjadi Werewolf!"
} else if (peran == "Guard") {
	var msg_peran = "Halo "+peran+" "+nama+" kamu akan membantu melindungi temanmu dari serangan Werewolf!"
} else if (peran == "Werewolf") {
	var msg_peran = "Halo "+peran+" "+nama+" kamu akan memakan mangsa setiap malam!"
}
console.log(msg_nama)
console.log(msg_peran)

//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
var nama = "Junaedi"
var peran = "Werewolf"

if ( nama.length == 0 ) {
    var msg_nama = "Nama harus diisi!"
} else {
	var msg_nama = "Selamat Datang di dunia Werewolf, "+nama
}

if ( peran.length == 0 ) {
    var msg_peran = "Pilih peranmu untuk memulai game!"
} else if (peran == "Penyihir") {
	var msg_peran = "Halo "+peran+" "+nama+" kamu dapat melihat siapa yang menjadi Werewolf!"
} else if (peran == "Guard") {
	var msg_peran = "Halo "+peran+" "+nama+" kamu akan membantu melindungi temanmu dari serangan Werewolf!"
} else if (peran == "Werewolf") {
	var msg_peran = "Halo "+peran+" "+nama+" kamu akan memakan mangsa setiap malam!"
}
console.log(msg_nama)
console.log(msg_peran)

/* 
Soal No. 6
B. Tugas Conditional

Switch case
*/


console.log(" ")
console.log("B. Tugas Conditional - Switch Case")

var tanggal=17; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan=8; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun=1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)


if (tanggal >=1 && tanggal <=31 ) {
	msg_tanggal = tanggal
}
else {
	msg_tanggal = false 
}


switch(bulan) {
  case 1:   { msg_bulan = "January"; break; }
  case 2:   { msg_bulan = "February"; break; }
  case 3:   { msg_bulan = "March"; break; }
  case 4:   { msg_bulan = "April"; break; }
  case 5:   { msg_bulan = "May"; break; }
  case 6:   { msg_bulan = "June"; break; }
  case 7:   { msg_bulan = "July"; break; }
  case 8:   { msg_bulan = "August"; break; }
  case 9:   { msg_bulan = "September"; break; }
  case 10:   { msg_bulan = "October"; break; }
  case 11:   { msg_bulan = "November"; break; }
  case 12:   { msg_bulan = "December"; break; }

  default:  { msg_bulan = false}}

if (tahun >=1900 && tahun <=2200 ) {
	msg_tahun = tahun
}
else {
	msg_tahun = false 
}


if (msg_tanggal == false || msg_bulan ==false || msg_tahun == false) {
	console.log("tanggal is", msg_tanggal)
	console.log("bulan is", msg_bulan)
	console.log("tahun is", msg_tahun)
} else
	console.log(msg_tanggal+" "+msg_bulan+" "+msg_tahun)