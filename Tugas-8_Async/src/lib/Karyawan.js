export default class Karyawan {
  constructor(name,password,admin) {
    this._name = name;
    this._password = password;
    this._role = admin;
    this._isLogin = false;
    this._students = [];
  }
  
  get name() {
    return this._name;
  
  }
  set name(name) {
    this._name =  name;
  }

  get password() {
    return this._password;
  
  }
  set password(password) {
    this._password =  password;
  }

  get role() {
    return this._role;
  }
  set role(role) {
    this._role =  role;
  }

  get isLogin() {
    return this._role;
  }
  set isLogin(isLogin) {
    this._isLogin =  isLogin;
  }

  get students() {
    return this._students;
  
  }
  set students(students) {
    this._students =  students;
  }

}