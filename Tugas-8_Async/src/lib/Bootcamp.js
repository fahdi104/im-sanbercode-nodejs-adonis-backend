import Karyawan from './Karyawan.js'
import fs from 'fs';;
import "core-js/stable";
import fsPromises from 'fs/promises';

const path = './src/data.json'

export default class Bootcamp {
  constructor(name) {
    this.name = name;
  }
  get name() {
    return this._name;
  }
  set name(name) {
    this._name = name;
  }

  register(input){
    let [name,password,role] = input.split(",")

    fs.readFile(path,(err,data) => {
      if(err){
        console.log(err)
      }

      let existingData = JSON.parse(data)
      let karyawan = new Karyawan(name,password,role)
      existingData.push(karyawan)

      fs.writeFile(path, JSON.stringify(existingData), err => {
        if (err) {
          console.error(err);
        }else{
          console.log('Berhasil Register')
        }
      });//end writefile

    });//end readfile

  }//end method register

  login(input){
    let [name,password] = input.split(",")

    fsPromises.readFile(path).then((data) => {
      let existingData = JSON.parse(data)

      //check if password is correct
      let karyawanID = existingData.findIndex((Karyawan) => Karyawan._name === name)
      let employee = existingData[karyawanID]
      
      if(karyawanID==-1)
      {
        console.log("Error: Data tidak ditemukan")
      }else if(employee._name==name && employee._password==password){
          employee._isLogin = true;

          //write to file
          existingData.splice(karyawanID,1,employee)
          return fsPromises.writeFile(path, JSON.stringify(existingData));
      }else{
        console.log("password error")
      }
      
    })
    .then(()=>{
      console.log('Berhasil Login');
    })
    .catch((err)=>{
      console.log(err)
    });// end promise sequence

  }//end method login

  async addSiswa(input){

  let [name,trainer] = input.split(",")

  let readFile = await fsPromises.readFile(path)
  let existingData = JSON.parse(readFile)

  //check if Admin is login
  let karyawanID = existingData.findIndex((Karyawan) => Karyawan._role === 'admin')
  let admin = existingData[karyawanID]

  if(karyawanID==-1)
      {
        console.log("Error: User Admin tidak ditemukan")
      }else if(admin._role=='admin' && admin._isLogin==true){

          //write to file
          let trainerID = existingData.findIndex((Karyawan) => Karyawan._name === trainer)
          let trainerEmp = existingData[trainerID]
          //trainerEmp._students = []
          trainerEmp._students.push({name})
          
          existingData.splice(trainerID,1,trainerEmp)

          //console.log(existingData)
          await fsPromises.writeFile(path, JSON.stringify(existingData));
          console.log("Berhasil add siswa ")
      }else{
        console.log("Error: Admin tidak login")
      }
    
  }//end method addSiswa

}//end class bootcamp