"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var Karyawan = /*#__PURE__*/function () {
  function Karyawan(name, password, admin) {
    _classCallCheck(this, Karyawan);

    this._name = name;
    this._password = password;
    this._role = admin;
    this._isLogin = false;
    this._students = [];
  }

  _createClass(Karyawan, [{
    key: "name",
    get: function get() {
      return this._name;
    },
    set: function set(name) {
      this._name = name;
    }
  }, {
    key: "password",
    get: function get() {
      return this._password;
    },
    set: function set(password) {
      this._password = password;
    }
  }, {
    key: "role",
    get: function get() {
      return this._role;
    },
    set: function set(role) {
      this._role = role;
    }
  }, {
    key: "isLogin",
    get: function get() {
      return this._role;
    },
    set: function set(isLogin) {
      this._isLogin = isLogin;
    }
  }, {
    key: "students",
    get: function get() {
      return this._students;
    },
    set: function set(students) {
      this._students = students;
    }
  }]);

  return Karyawan;
}();

exports["default"] = Karyawan;