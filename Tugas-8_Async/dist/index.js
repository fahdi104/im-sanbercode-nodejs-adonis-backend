"use strict";

var _Bootcamp = _interopRequireDefault(require("./lib/Bootcamp.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var myArgs = process.argv.slice(2);
var sanber = new _Bootcamp["default"]("sanbercode");

switch (myArgs[0]) {
  case 'register':
    var input = myArgs[1];
    sanber.register(input);
    break;

  case 'login':
    var login = myArgs[1];
    sanber.login(login);
    break;

  case 'addSiswa':
    var siswa = myArgs[1];
    sanber.addSiswa(siswa);
    break;

  default:
    console.log('Sorry, that is not something I know how to do.');
}