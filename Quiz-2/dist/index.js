"use strict";

var _OlahString = _interopRequireDefault(require("./lib/OlahString"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var myArgs = process.argv.slice(2);

switch (myArgs[0]) {
  case 'testOlahString':
    var input = myArgs[1];

    _OlahString["default"].palindrome('katak'); // Output: true


    _OlahString["default"].palindrome('sanbers'); // Output: false


    _OlahString["default"].ubahKapital('asep'); // Output : Asep


    _OlahString["default"].ubahKapital('abduh'); // Output: Abduh


    break;

  case 'hello':
    console.log("test hello");
    break;

  default:
    console.log('Sorry, that is not something I know how to do.');
}