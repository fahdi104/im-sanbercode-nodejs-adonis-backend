"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var data = {
  name: 'Bondra',
  nilai: 70,
  kelas: 'Adonis',
  isLogin: true
};

var updateData = function updateData(update) {
  //shallow(?)copy data, so original data is not changed in test case
  var output = _objectSpread({}, data);

  var name = output.name,
      nilai = output.nilai,
      kelas = output.kelas,
      isLogin = output.isLogin;

  for (var key in update) {
    output[key] = update[key];
  } //console.log(data)


  return output;
}; //test case


console.log(updateData({
  isLogin: false
}));
console.log(updateData({
  nilai: 65,
  kelas: 'Laravel'
}));