const data = { 
name: 'Bondra',
nilai:70,
kelas:'Adonis',
isLogin:true
}

const updateData = (update) => {
   
   //shallow(?)copy data, so original data is not changed in test case
	let output = {...data};

	let {name,nilai,kelas,isLogin} = output

    for (const key in update) {
    	output[key] = update[key]
	}
	
	//console.log(data)
	return output
}

//test case
console.log(updateData({ isLogin: false }))
console.log(updateData({nilai: 65, kelas: 'Laravel'}))