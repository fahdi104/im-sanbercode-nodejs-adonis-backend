import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class MoviesController {
  public async index({response}: HttpContextContract) {
    //@TODO join to get genre name
    try{
      //const values = await Database
      //.from('movies')
      //.select('*')

      const values = await Database
      .from('movies')
      .join('genres', 'movies.genre_id', '=', 'genres.id')
      .select('movies.*')
      .select('genres.name as genre')
      
      response.ok({
        status: "Query Success",
        genres: values
      })

    }catch(error){
      response.badRequest({
        status: "Query failed",
      })
    }
  }

  public async store({request, response}: HttpContextContract) {
    //movies : id (integer PK) , title(varchar), resume(text/longtext), 
    //release_date(datetime)
    try{
      await Database
        .table('movies') 
        .insert({ 
          title : request.input('title'),
          resume : request.input('resume'),
          release_date : request.input('release_date'),
          genre_id: request.input('genre_id')
        })

        response.ok({
          status: "Input Success",
        })

  }catch(error){
    response.badRequest({
      status: "Input failed",
    })  

  }

  public async show({response, params}: HttpContextContract) {
    try{
      const values = await Database
      .from('movies')
      .join('genres', 'movies.genre_id', '=', 'genres.id')
      .select('movies.id')
      .select('movies.title')
      .select('movies.resume')
      .select('movies.release_date')
      .select('genres.name as genre')
      .where('movies.id',params.id)

      response.ok({
        status: "Query Success",
        movie_detail : values
      })
    }catch(error){
      response.badRequest({
        status: "Query failed",
      })
    }
  }

  public async update({request, response, params}: HttpContextContract) {
    try{
      await Database
      .from('movies')
      .where('id', params.id)
      .update({ 
        title : request.input('title'),
        resume : request.input('resume'),
        release_date : request.input('release_date'),
        genre_id: request.input('genre_id')
      })
      response.ok({
        status: "Update Success",
        id: params.id,
      })
    }catch(error){
      response.badRequest({
        status: "Update failed",
      })
    } 
  }

 public async destroy({response, params}: HttpContextContract) {
    try{
      await Database
      .from('movies')
      .where('id', params.id)
      .delete()

      response.ok({
        status: "Delete Success",
        id: params.id
      })
    }catch(error){
      response.badRequest({
        status: "Delete failed",
        id: params.id
      })
    }
  }
}
