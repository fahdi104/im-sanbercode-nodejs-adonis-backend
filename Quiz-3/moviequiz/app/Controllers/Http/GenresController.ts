import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class GenresController {
  public async index({response}: HttpContextContract) {
    try{
      const values
       = await Database
      .from('genres')
      .select('*')
      
      response.ok({
        status: "Query Success",
        genres: values
      })

    }catch(error){
      response.badRequest({
        status: "Query failed",
      })
    }
  }

  public async store({request, response}: HttpContextContract) {
    //genres: id (integer PK), name (varchar)
    try{
      await Database
        .table('genres') 
        .insert({ 
          name : request.input('name')
        })

        response.ok({
          status: "Input Success",
        })
  }catch(error){
    response.badRequest({
      status: "Input failed",
    })  
  }

  public async show({response, params}: HttpContextContract) {
    try{
      //const values = await Database
      //.from('genres')
      //.where('id', params.id)
      
      // to return both genres and movies list, need to do 2 query
      
      const genres = await Database
      .from('genres')
      .select('id')
      .select('name')
      .where('id', params.id)
      .first()

      const movies = await Database
      .from('movies')
      .select('id')
      .select('title')
      .select('resume')
      .select('release_date')
      .whereIn(
        'genre_id',
        Database
          .from('genres')
          .select('id')
          .where('id',params.id)
      )
      response.ok({
        status: "Query Success",
        id : genres['id']
        name : genres['name']
        movies : movies
      })

    }catch(error){
      response.badRequest({
        status: "Query failed",
        message: error.message
      })
    }
  }

  public async update({request, response, params}: HttpContextContract) {
    try{

      await Database
      .from('genres')
      .where('id', params.id)
      .update({ 
        name : request.input('name')
      })

      response.ok({
        status: "Update Success",
        id: params.id,
      })

    }catch(error){
      response.badRequest({
        status: "Update failed",
      })
    } 
  }

  public async destroy({response, params}: HttpContextContract) {

    try{

      await Database
      .from('genres')
      .where('id', params.id)
      .delete()

      response.ok({
        status: "Delete Success",
        id: params.id
      })

    }catch(error){
      response.badRequest({
        status: "Delete failed",
        id: params.id
      })
    }

  }
}
