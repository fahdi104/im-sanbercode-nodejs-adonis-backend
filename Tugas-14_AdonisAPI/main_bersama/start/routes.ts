/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

Route.get('/test','VenuesController.test')

//release 0
//Route.post('/venues','VenuesController.register')

//release 1
//Route.post('/bookings','BookingsController.create')

//Tugas hari ke 15
//Venues
Route.resource('venues', 'VenuesController').apiOnly()

//Fields, Nested from velues
Route.resource('venues.fields', 'FieldsController').apiOnly()