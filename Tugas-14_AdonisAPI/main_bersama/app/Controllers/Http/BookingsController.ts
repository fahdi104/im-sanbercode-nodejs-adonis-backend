import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateBookingValidator from 'App/Validators/CreateBookingValidator'

export default class BookingsController {

    public async test({response}: HttpContextContract) {
        response.status(200).json({
            message :"test"
        })

    //release 1
    public async create({request,response}: HttpContextContract) {
        try{
            const values = await request.validate(CreateBookingValidator)
            response.ok({
                message: values
            })

            //response.status(200).json({
            //    message :"test"
            //})

        }catch(error){
            response.badRequest(error.messages)
        }
    }   

}
