import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

//validator
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'

export default class VenuesController {

  public async index({request, response}: HttpContextContract) {
    try{
      const values = await Database
      .from('venues')
      .select('*')
      
      response.ok({
        status: "Query Success",
        venues: values
      })

    }catch(error){
      response.badRequest({
        status: "Query failed",
        message: error.messages
      })
    }
  }

  public async store({request, response}: HttpContextContract) {
    try{
      const values = await request.validate(CreateVenueValidator)
      await Database
        .table('venues') // 👈 gives an instance of insert query builder
        .insert({ 
          name : values.name,
          address : values.address,
          phone :values.phone
        })

        response.ok({
          status: "Input Success",
          venues: values
        })
  }catch(error){
    response.badRequest({
      status: "Input failed",
      message: error.messages
    })  
  }

  }

  public async show({response, params}: HttpContextContract) {
    try{
      const values = await Database
      .from('venues')
      .where('id', params.id)
      .first()
      
      response.ok({
        status: "Query Success",
        venues: values
      })
    }catch(error){
      response.badRequest({
        status: "Query failed",
        message: error.messages
      })
    }

  }

  public async update({request, response, params}: HttpContextContract) {
    try{
      const values = await request.validate(CreateVenueValidator)

      await Database
      .from('venues')
      .where('id', params.id)
      .update({ 
        name : values.name,
        address : values.address,
        phone :values.phone
      })

      response.ok({
        status: "Update Success",
        id: params.id,
        venues: values
      })

    }catch(error){
      response.badRequest({
        status: "Update failed",
        message: error.messages
      })
    } 
  }

  public async destroy({response, params}: HttpContextContract) {
    try{

      await Database
      .from('venues')
      .where('id', params.id)
      .delete()

      response.ok({
        status: "Delete Success",
        id: params.id
      })

    }catch(error){
      response.badRequest({
        status: "Delete failed",
        message: error.messages,
        id: params.id
      })
    }

  }
}
