import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class FieldsController {
  public async index({response, params}: HttpContextContract) {

    try{
      //if tables have same name, Adonis output it as same name, useless join
      //const values = await Database
      //.from('fields')
      //.join('venues', 'fields.venue_id', '=', 'venues.id')
      //.select('fields.*')
      //.select('venues.id' as 'gogoID')
      //.where('venue_id', params.venue_id)

      //simple one
      const values = await Database
      .from('fields')
      .select('fields.*')
      .where('venue_id', params.venue_id)
      
      response.ok({
        status: "Query Success",
        venues: values
      })

    }catch(error){
      response.badRequest({
        status: "Query failed",
        message: error.messages
      })
    }

  }

  public async store({request, response, params}: HttpContextContract) {
    try{
      await Database
        .table('fields') 
        .insert({ 
          name : request.input('name'),
          type : request.input('type'),
          venue_id : params.venue_id
        })

        response.ok({
          status: "Input Success"
        })
  }catch(error){
    response.badRequest({
      status: "Input failed",
      message: error.messages
    })  

  }

  public async show({response, params}: HttpContextContract) {

    try{
      const values = await Database
      .from('fields')
      .select('*')
      .where('venue_id',params.venue_id)
      .andWhere('id', params.id)

      response.ok({
        status: "Query Success",
        venues: values
      })

    }catch(error){
      response.badRequest({
        status: "Query failed",
        message: error.messages
      })
    }
    
  }

  public async update({request, response, params}: HttpContextContract) {
    try{
      await Database
        .from('fields') 
        .where('id', params.id)
        .andWhere('venue_id', params.venue_id)
        .update({ 
          name : request.input('name'),
          type : request.input('type'),
        })

        response.ok({
          status: "Update Success"
        })
  }catch(error){
    response.badRequest({
      status: "Update failed",
      message: error.messages
    })
  }

  public async destroy({response,params}: HttpContextContract) {

    try{
      await Database
        .from('fields') 
        .where('id', params.id)
        .andWhere('venue_id', params.venue_id)
        .delete()

        response.ok({
          status: "Delete Success"
        })
  }catch(error){
    response.badRequest({
      status: "Delete failed",
      message: error.messages
    })
  }
}