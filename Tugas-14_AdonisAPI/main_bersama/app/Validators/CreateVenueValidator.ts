import { schema, rules, CustomMessages } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class CreateVenueValidator {
  constructor(protected ctx: HttpContextContract) {}

  /*
   * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
   *
   * For example:
   * 1. The username must be of data type string. But then also, it should
   *    not contain special characters or numbers.
   *    ```
   *     schema.string({}, [ rules.alpha() ])
   *    ```
   *
   * 2. The email must be of data type string, formatted as a valid
   *    email. But also, not used by any other user.
   *    ```
   *     schema.string({}, [
   *       rules.email(),
   *       rules.unique({ table: 'users', column: 'email' }),
   *     ])
   *    ```
   */
  public schema = schema.create({
    name: schema.string([
      rules.minLength(5),
      rules.maxLength(200),
      rules.trim(),
      rules.escape()
    ]),
    address: schema.string([
      rules.minLength(10),
      rules.maxLength(200),
      rules.trim(),
      rules.escape()
    ]),
    phone: schema.string([
      rules.mobile({
        locales: ['en-ID', 'en-US']
      })
    ])
  })

  /**
   * Custom messages for validation failures. You can make use of dot notation `(.)`
   * for targeting nested fields and array expressions `(*)` for targeting all
   * children of an array. For example:
   *
   * {
   *   'profile.username.required': 'Username is required',
   *   'scores.*.number': 'Define scores as valid numbers'
   * }
   *
   */
  public messages: CustomMessages = {
    required: 'The {{ field }} cannot be empty',
    'nama.alpha': '{{ field }} Only alphabet',
    'name.minLength' : '{{ field }} Min 5 Characters',
    'alamat.minLength' : '{{ field }} Min 10 Characters',
    'phone.mobile' : '{{ field }} Invalid mobile'
  }
}
