import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'fields'

  //Release 0 Setup & Migrations

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      //fields berisi data: id, name, type (string) , venue_id. 
      table.increments('id')
      table.string('name').notNullable
      table.enum('type',['futsal','mini soccer','basketball']).notNullable
      table.integer('venue_id')
        .unsigned()
        .references('venues.id')
        .onDelete('CASCADE')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps(true, true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
