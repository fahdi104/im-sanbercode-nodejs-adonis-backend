/*
No 1
*/

console.log("Soal 1")

var step = 2;
var stop = 20;

console.log("LOOPING PERTAMA")
while(step < stop) { 
  console.log(step + ' - I love coding'); // Menampilkan nilai flag pada iterasi tertentu
  step += 2
}
var step = 20
var stop = 0
console.log("LOOPING KEDUA")
while(step > 0) { 
  console.log(step + ' - I will become a mobile developer'); // Menampilkan nilai flag pada iterasi tertentu
  step -= 2
}

/*
No 2

SYARAT: 
A. Jika angka ganjil maka tampilkan Santai 
B. Jika angka genap maka tampilkan Berkualitas 
C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.

*/
console.log(" ")
console.log("Soal 2")

for(var angka = 1; angka <= 20; angka++) {
  
  if (angka % 2 != 0 && angka % 3 != 0){
    var msg = "Santai"
  } else if (angka % 2 == 0) { 
    var msg = "Berkualitas"
  } else if (angka % 3 == 0 && angka % 2 != 0) { 
    var msg = "I Love Coding"
  }
  console.log(angka +" - " + msg );
} 


/*
No. 3 Membuat Persegi Panjang #

*/
console.log(" ")
console.log("Soal 3")

function makeRectangle(panjang,lebar) {
  var str = '';
  for(var i = 0; i < lebar; i++) {
    for(var j=0; j < panjang; j++){
          str += "#";
    }
    str += "\n";
  }
  return str
}

// TEST CASE
console.log(makeRectangle(8,4))


/*
No. 4 Membuat Tangga

*/
console.log(" ")
console.log("Soal 4")

function makeLadder(sisi) {
  var str = '';
  for(var i = 0; i < sisi; i++) {
    for(var j=0; j <= i; j++){
          str += "#";
    }
    str += "\n";

  }
  return str
}

// TEST CASE
console.log(makeLadder(7))