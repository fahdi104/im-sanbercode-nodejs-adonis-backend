/* 
Soal No. 1 (Membuat kalimat), 

*/
console.log("Soal 1")


function teriak() {
  return "Halo Sanbers!"
}

// TEST CASE
console.log(teriak()) // "Halo Sanbers!" 


/*
No. 2
Tulislah sebuah function dengan nama kalikan() yang 
mengembalikan hasil perkalian dua parameter yang di kirim melalui process.argv
*/
console.log(" ")
console.log("Soal 2")

function kalikan(num1, num2) {
  return num1*num2
}

console.log(kalikan(4, 12)) // 48


/*
No. 3
Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim 
menjadi sebuah kalimat perkenalan seperti berikut: 
"Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!
*/
console.log(" ")
console.log("Soal 3")

function introduce(name, age, address, hobby) {
  if (name == undefined || age == undefined || address == undefined || hobby == undefined) {
    var msg = "Error, need input (name, age, address, hobby)";
  } else {
  	var msg="Nama Saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+" dan saya punya hobby yaitu "+hobby+"!"
  } 
  return msg
}

 
 // TEST CASES
console.log(introduce("Agus", 30, "Jogja", "Gaming")) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jogja, dan saya punya hobby yaitu Gaming!" 
console.log(introduce())
