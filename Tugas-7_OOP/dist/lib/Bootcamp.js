"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _Kelas = _interopRequireDefault(require("./Kelas.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var Bootcamp = /*#__PURE__*/function () {
  function Bootcamp(name) {
    _classCallCheck(this, Bootcamp);

    this.name = name;
    this._kelas = [];
  }

  _createClass(Bootcamp, [{
    key: "name",
    get: function get() {
      return this._name;
    },
    set: function set(name) {
      this._name = name;
    }
  }, {
    key: "classes",
    get: function get() {
      return this._kelas;
    }
  }, {
    key: "createClass",
    value: function createClass(name, level, instructor) {
      var newKelas = new _Kelas["default"](name, level, instructor);

      this._kelas.push(newKelas);
    }
  }, {
    key: "register",
    value: function register(kelas, newStud) {
      var kelasID = this._kelas.find(function (Kelas) {
        return Kelas._name === kelas;
      });

      kelasID.addStudentToClass(newStud);
    }
  }]);

  return Bootcamp;
}();

exports["default"] = Bootcamp;