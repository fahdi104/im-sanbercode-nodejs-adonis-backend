"use strict";

var _Bootcamp = _interopRequireDefault(require("./lib/Bootcamp.js"));

var _Kelas = _interopRequireDefault(require("./lib/Kelas.js"));

var _Student = _interopRequireDefault(require("./lib/Student.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var sanber = new _Bootcamp["default"]("sanbercode"); //sanber.createClass("Laravel", "beginner", "abduh")

sanber.createClass("Laravel", "beginner", "abduh");
sanber.createClass("React", "beginner", "abdul"); //sanber.createClass("React", "beginner", "abdul")
//console.log(sanber.classes)
//release 1

var names = ["regi", "ahmad", "bondra", "iqbal", "putri", "rezky"];
names.map(function (nama, index) {
  var newStud = new _Student["default"](nama);
  var kelas = sanber.classes[index % 2].name;
  sanber.register(kelas, newStud);
}); //menampilkan data kelas dan student nya

sanber.classes.forEach(function (kelas) {
  console.log(kelas);
});