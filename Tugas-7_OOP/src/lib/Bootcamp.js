import Kelas from './Kelas.js'

export default class Bootcamp {
  constructor(name) {
    this.name = name;
    this._kelas = [];
  }
  get name() {
    return this._name;
  }
  set name(name) {
    this._name = name;
  }

  get classes(){
    return this._kelas;
  }

  createClass(name,level,instructor){
    let newKelas = new Kelas(name,level,instructor)
    this._kelas.push(newKelas) 
  }

  register(kelas,newStud){
    let kelasID = this._kelas.find((Kelas) => Kelas._name === kelas)
    kelasID.addStudentToClass(newStud);
  }
}