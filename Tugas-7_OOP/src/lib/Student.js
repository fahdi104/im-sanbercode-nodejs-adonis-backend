export default class Student {
  constructor(student) {
    this._name = student;
  }
  
  get name() {
    return this._name;
  
  }
  set name(name) {
    this._name =  name;
  }
}