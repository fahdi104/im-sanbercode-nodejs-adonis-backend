export default class Kelas {
  constructor(name,level,instructor) {
    this._name = name;
    this._students = [];
    this._level = level;
    this._instructor = instructor; 

  }
  get name() {
    return this._name;
  
  }
  set name(name) {
    this._name =  name;
  }

  get students() {
    return this._students;
  
  }
  set students(students) {
    this._students =  students;
  }

  get level() {
    return this._level;
  
  }
  set level(level) {
    this._level =  level;
  }
  
  get instructor() {
    return this._instructor;
  
  }
  set level(instructor) {
    this._instructor =  instructor;
  }

  addStudentToClass(objStudent){
    this._students.push(objStudent);
  }

}